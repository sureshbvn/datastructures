
public class MoveZeros {
/***
 * 
 * 
 * Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

 * For example, given nums = [0, 1, 0, 3, 12], after calling your function, nums should be [1, 3, 12, 0, 0].
 * @param nums
 */
	
	    public void moveZeroes(int[] nums) {
	        int z=0;
	        for(int i=0;i<nums.length;i++){
	            int temp=nums[i];
	            if(temp!=0){
	                nums[i]=nums[z];
	                nums[z]=temp;
	                z++;
	            }
	        }
	    }

}
