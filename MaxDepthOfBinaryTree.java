import java.util.Deque;
import java.util.LinkedList;

public class MaxDepthOfBinaryTree {

	public int maxDepth(TreeNode root){
		int count=0;
		if(root==null){
			return 0;
		}
		else{
			//return 1+Math.max(maxDepth(root.left), maxDepth(root.right));
			Deque<TreeNode> stack=new LinkedList<TreeNode>();
			stack.push(root);
			
			while(!stack.isEmpty()){
				int size=stack.size();
				while(size-->0){
				    TreeNode cur=stack.pop();
					if(cur.left!=null){
						stack.addLast(cur.left);
					}
					if(cur.right!=null){
						stack.addLast(cur.right);
					}
				
				}
				count++;
			}
		}
		
		return count;
	}
	
}

class TreeNode{
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode(int x){
		val=x;
	}
}