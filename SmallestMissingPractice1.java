package Arrays;

public class SmallestMissingPractice1 {

	static int  smallestNumber(int[] arr){
	    
        boolean[] b=new boolean[arr.length];
        b[0]=true;
        int size=arr.length;
        
        int min=Integer.MAX_VALUE;
        
        //code to find the minimum
        for(int i=0;i<size;i++){
            if(arr[i]<min)
                min=arr[i];
        
        }
        //find the minmum offset from the minimum
        
        for(int i=0;i<size;i++){
        
            if((arr[i]-min)<size){
                b[arr[i]-min]=true;
            
            }
            
            
        }
        
        //get the minimum element
        for(int i=1;i<size;i++){
             if(!b[i]){
                 return i+min;
             }
        
        
        }
        return 0;
        
    }
    public static void main(String args[]){
    
        int[] a={-3,-43,-32,1,2};
        
        System.out.println(smallestNumber(a));
    }
}
