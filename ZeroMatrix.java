package Arrays;

public class ZeroMatrix {

	static void nullifyrow(int[][] matrix, int row) {
		for (int i = 0; i < matrix[0].length; i++) {
			matrix[row][i] = 0;
		}
	}

	static void nullifyColumn(int[][] matrix, int column) {
		for (int i = 0; i < matrix.length; i++) {
			matrix[i][column] = 0;
		}
	}

	static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {

				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}

	static void setzeros(int[][] matrix) {
		boolean rowZero = false;
		boolean columnZero = false;

		for (int i = 0; i < matrix[0].length; i++) {
			if (matrix[0][i] == 0) {
				rowZero = true;
				break;
			}
		}
		for (int i = 0; i < matrix.length; i++) {
			if (matrix[i][0] == 0) {
				columnZero = true;
				break;
			}
		}

		for (int i = 1; i < matrix.length; i++) {

			for (int j = 1; j < matrix[0].length; j++) {
				if (matrix[i][j] == 0) {
					matrix[0][j] = 0;
					matrix[i][0] = 0;
				}

			}
		}
		
		for(int i=0;i<matrix.length;i++){
			if(matrix[i][0]==0){
				nullifyrow(matrix,i);
			}
		}
		for(int i=0;i<matrix[0].length;i++){
			if(matrix[0][i]==0){
				nullifyColumn(matrix,i);
			}
		}
		
		
		if(rowZero){
			nullifyrow(matrix, 0);
		}
		if(columnZero){
			nullifyColumn(matrix, 0);
		}

	}

	public static void main(String args[]) {
		int[][] matrix = new int[3][4];
		matrix[0][0] = 1;
		matrix[0][1] = 0;
		matrix[0][2] = 0;
		matrix[0][3] = 0;

		matrix[1][0] = 1;
		matrix[1][1] = 0;
		matrix[1][2] = 1;
		matrix[1][3] = 1;

		matrix[2][0] = 1;
		matrix[2][1] = 1;
		matrix[2][2] = 1;
		matrix[2][3] = 1;

		printMatrix(matrix);
		
		setzeros(matrix);
		System.out.println();
		printMatrix(matrix);

	}
}
