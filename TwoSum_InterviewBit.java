package TwoPointers;

import java.util.ArrayList;
import java.util.List;

public class TwoSum_InterviewBit {

	public static boolean twoSum(List<Integer> list, int target) {

		int right = list.size() - 1;

		for (int left = 0; left < list.size() - 1; left++) {

			for (; right >= 0; right--) {

				if (left != right && (list.get(left) + list.get(right) == target))
					return true;
				if (list.get(left) + list.get(right) < 0)
					break;
			}

		}
		return false;

	}
	public static void main(String args[]){
		List<Integer> list=new ArrayList<>();
		list.add(-5);
		list.add(1);
		list.add(5);
		System.out.println(new TwoSum_InterviewBit().twoSum(list, 1));
	}

}
