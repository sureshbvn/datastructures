
/**
 * 
 *  4
   /   \
  2     7
 / \   / \
1   3 6   9


     4
   /   \
  7     2
 / \   / \
9   6 3   1
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class InvertBinaryTree {
    public TreeNode invertTree(TreeNode root) {
        TreeNode node=root;
        if(node==null)
        return node;
        
        else{
            TreeNode temp=node.left;
            node.left=invertTree(node.right);
            node.right=invertTree(temp);
            return node;
        }
    }
}