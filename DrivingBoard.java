package ModerateProblems;

import java.util.HashSet;

/*
 * 
 * You are building a board by placing a bunch of planks of wood end to end.THere are two types
 * of plank one of length shorter  and one of length longer. You must use exactly kp lanks of wood.
 * Write a method to generate all possible lengths for the driving board.
 * 
 * 
 */

public class DrivingBoard {

	/*
	 * 
	 * This is a traditional brute force approach and the complexity is 2^k
	 * since there are two choices in each recursive call;
	 */

	public HashSet<Integer> allLengthSBruteForceSolution(int k, int shorter, int longer) {

		HashSet<Integer> lengths = new HashSet<Integer>();
		callBruteForceRecursion(k, 0, shorter, longer, lengths);
		return lengths;

	}

	public void callBruteForceRecursion(int k, int total, int shorter, int longer, HashSet<Integer> lengths) {
		if (k == 0) {
			lengths.add(total);
			return;
		}
		callBruteForceRecursion(k - 1, total + shorter, shorter, longer, lengths);
		callBruteForceRecursion(k - 1, total + longer, shorter, longer, lengths);

	}

	/*
	 * 
	 * 
	 * This is careful brute force and complexity is O(K^2). We have k slacs and each have
	 * possible length of k*(k*longer). by careful elimnation we are reducing it
	 * to O(K^2)
	 */
	public HashSet<Integer> allLengthsCareful(int k, int shorter, int longer) {
		HashSet<Integer> lengths = new HashSet<Integer>();
		HashSet<String> visited = new HashSet<String>();
		careFulBruteForce(k, 0, shorter, longer, lengths, visited);
		return lengths;
	}

	public void careFulBruteForce(int k, int total, int shorter, int longer, HashSet<Integer> lengths,
			HashSet<String> visited) {
		if (k == 0) {
			lengths.add(total);
			return;
		}
		String key = k + " " + total;
		if (visited.contains(key)) {
			return;

		}
		careFulBruteForce(k, total + shorter, shorter, longer, lengths, visited);
		careFulBruteForce(k, total + longer, shorter, longer, lengths, visited);

	}

	
	/***
	 * optimized solution
	 * 
	 * It is only possible to have k different sums
	 * 
	 * like 
	 * 
	 * 0 Of shorter k of longer, 1 of shorter, k-1 of longer
	 */
	
	HashSet<Integer> optimized(int k,int shorter,int longer){
		HashSet<Integer> set=new HashSet<>();
		for(int i=0;i<=k;i++){
			int value=i*i+(k-i)*(k-i);
			set.add(value);
		}
		return set;
	}
	
	
	
}
