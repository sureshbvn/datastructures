package ModerateProblems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/*
 * 
 * 
 * This is problem number 24 in moderate section.
 * 
 * 16.24 and page number 520
 */


class Pair{
	
	int x;
	int y;
	public Pair(int x,int y){
		this.x=x;
		this.y=y;
	}
}

public class PairsSum {
	
	
	/*
	 * This is traditional brute force approach with complexity o(n^2) and no extra space
	 * 
	 * The disadvantage of this solution is it prints duplicates
	 */
	public ArrayList<Pair> bruteForceApproach(int[] array,int sum){
		ArrayList<Pair> result=new ArrayList<Pair>();
		for(int i=0;i<array.length;i++){
			int val=array[i];
			for(int j=0;j<array.length;j++){
				int val2=array[j];
				if(val+val2==sum){
					result.add(new Pair(val, val2));
				}
			}
		}
		return result;
	}
	
	/**
	 * 
	 * 
	 * 
	 * @param args
	 * Thiis is an effecient solution using Hash set. It uses extra O(N) space. but the runtime
	 * is o(n). This solution does not allow duplicates
	 * 
	 * 
	 * 
	 */
	public ArrayList<Pair> optimizedApproach(int[] array,int sum){
		
		ArrayList<Pair> result=new ArrayList<Pair>();
		HashSet<Integer> set=new HashSet<Integer>();
		
		for(int x:array){
			
			int compliment=sum-x;
			if(set.contains(compliment)&&!set.contains(x)){
				result.add(new Pair(x,compliment));
				
			}
			set.add(x);
		}
		return result;
	}
	
	/****
	 * 
	 * 
	 * @param args
	 * This is an alternative solution. It uses O(Nlongn) complexity to sort and O(N) to get the pairs.
	 * 
	 * There is no extra space.
	 * 
	 * 
	 */
	
	public ArrayList<Pair> alternateApproach(int[] array,int sum){
		Arrays.sort(array);//costly step.
		ArrayList<Pair> result=new ArrayList<Pair>();
		int first=0;
		int last=array.length-1;
		while(first<last){
			int firstValue=array[first];
			int secondValue=array[last];
			if(firstValue+secondValue==sum){
				result.add(new Pair(firstValue,secondValue));
				first++;
				last--;
				
			}
			else{
				if(firstValue+secondValue>sum){
					last--;
				}
				else{
					first++;
				}
			}
			
			
		}
		return result;
		
		
		
	}
	
	
	public static void main(String args[]){
		
		int[] array={2,3,1,4};
		int sum=5;
		//ArrayList<Pair> result=new PairsSum().bruteForceApproach(array, sum);
		//ArrayList<Pair> result=new PairsSum().optimizedApproach(array, sum);
		ArrayList<Pair> result=new PairsSum().alternateApproach(array, sum);
		for(Pair p:result){
			System.out.println(p.x+" "+p.y);
		}
	}
	
	
  

}
