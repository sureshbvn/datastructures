
public class StepsCount {

	private int stepCount(int n){
		if(n==0 || n==1 || n==2)
			return n;
		else{
			int prev=1;
			int next=2;
			int sum=0;
			for(int i=3;i<=n;i++){
				sum=prev+next;
				prev=next;
				next=sum;
				
			}
			return sum;
		}
	}
	public static void main(String args[]){
		int n=4;
		StepsCount problemObject=new StepsCount();
		int count=problemObject.stepCount(n);
		System.out.println(count);
	}
}
