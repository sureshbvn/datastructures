
/*
 * 
 * Given a column title as appear in an Excel sheet, return its corresponding column number.
 * 
 *  A -> 1
    B -> 2
    C -> 3
    ...
    Z -> 26
    AA -> 27
    AB -> 28 
 */
public class ExcelSheetColumnNumber {
    public int titleToNumber(String s) {
        int len=s.length();
        int result=0;
       for(int i=0;i<len;i++){
           result=(result*26)+((s.charAt(i)-'A')+1);
       }
       return result;
    }
}
