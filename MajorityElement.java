
public class MajorityElement {
	 /**
	 * @param nums
	 * @return
	 */
	public int majorityElement(int[] nums) {
	int majorityElement=nums[0];
	int count=0;
	for(int counter=0;counter<nums.length;counter++){
		//case1:
		if(count==0){
			count++;
			majorityElement=nums[counter];
		}
		//case 2:
		else if(majorityElement==nums[counter]){
			count++;
		}
		//case 3:
		else{
			count--;
		}
		
	}
	return majorityElement;
		 
	    }
	public static void main(String args[]){
		int[] arr={2,1,1,1,1,1,2};
		MajorityElement problemObject=new MajorityElement();
		System.out.println(problemObject.majorityElement(arr));
	
}
}
