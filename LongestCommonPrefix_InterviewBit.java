package Strings;

import java.util.ArrayList;

public class LongestCommonPrefix {
	
	public String longestCommonPrefix(ArrayList<String> A) {
		
		if(A.size()==0){
			return "";
		}
		//finding minimum length String
		
		int minimumLength=Integer.MAX_VALUE;
		
		for(int i=0;i<A.size();i++){
			minimumLength=Math.min(minimumLength,A.get(i).length());
		}
		
		StringBuffer result=new StringBuffer();
		
		for(int i=0;i<minimumLength;i++){
			char c=A.get(0).charAt(i);
			for(int j=1;j<A.size();j++){
				if(c!=A.get(j).charAt(i)){
					return result.toString();
				}
				
					
				
			}
		result.append(c);
			
		}
		return result.toString();
		
	}
	
	public static void main(String args[]){
		LongestCommonPrefix prefix=new LongestCommonPrefix();
		
		ArrayList<String> list=new ArrayList<>();
		
		list.add("abcd");
		list.add("abcde");
		list.add("abcdef");
		
		System.out.println(prefix.longestCommonPrefix(list));
	}
	

}
