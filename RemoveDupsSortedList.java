/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class RemoveDupsSortedList {
    public ListNode deleteDuplicates(ListNode head) {
    	ListNode tempNode=head;
    	if(tempNode==null){
    		return null;
    	}
    	else{
    		while(tempNode.next!=null){
    			if(tempNode.val==tempNode.next.val){
    				tempNode.next=tempNode.next.next;
    			}
    			else{
    				tempNode=tempNode.next;
    			}
    		}
    	}
    	return head;
        
    }
    public static void main(String args[]){
    	ListNode head=new ListNode(1);
    	ListNode node1=new ListNode(1);
    	head.next=node1;
    	node1.next=new ListNode(2);
    	RemoveDupsSortedList problemObject=new RemoveDupsSortedList();
    	problemObject.deleteDuplicates(head);
    	
    }
}