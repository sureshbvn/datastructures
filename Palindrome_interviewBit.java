package Strings;



public class Palindrome {
	
	
	/**
	 * @param inputString to be checked wheather a palindrome or not
	 * @return 1 if string is palindrome
	 * @return 0 if string is not palindrome
	 * 
	 * Boundary Cases:
	 * 1) If String is empty return 1. Because empty string is palindrome.
	 * 2) We are only considering alpha numeric characters.
	 * 
	 * Key points
	 * 1)Try to avoid working directly on string. String literal operations are costly in java.
	 * 
	 * Logic
	 * 1)Remove the characters other than alpha numeric characters.
	 * 
	 * 2)Like binary search, start a pointer from the beg and end. Check the characters if they are equal
	 * increment the left pointer and decrement the right pointer
	 * 
	 * 3)Return 0 otherwise.
	 * 
	 * 
	 */
	public int isPalindrome(String inputString){
		if(inputString.length()==0){
			return 1;
		}
		
		
		StringBuffer sb=new StringBuffer();
		
		for(int i=0;i<inputString.length();i++){
			if((inputString.charAt(i)>='a'&&inputString.charAt(i)<='z') ||(inputString.charAt(i)>='A'&&inputString.charAt(i)<='Z')||(inputString.charAt(i)>=0 &&inputString.charAt(i)<=9)){
				sb.append(Character.toLowerCase(inputString.charAt(i)));
				}
		}
		
		String modifiedString=sb.toString();
		
		
		int n=modifiedString.length();
		
		for(int i=0;i<n/2;i++){
			
			if(modifiedString.charAt(i)!=modifiedString.charAt(n-i-1)){
				return 0;
			}
			
		}
		
		
		return 1;
		
	}
	public static void main(String args[]){
		Palindrome p=new Palindrome();
		System.out.println(p.isPalindrome("A man, a plan, a canal: Panama"));
		System.out.println(p.isPalindrome("race a car"));
	}

}
