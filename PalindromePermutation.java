package LeetCode;

import java.util.HashMap;
import java.util.HashSet;

/*
 * LeetCode problems
 * Given a string, determine if a permutation of the string could form a palindrome.

	For example,
	"code" -> False, "aab" -> True, "carerac" -> True.
 */

public class PalindromePermutation {

	/*
	 * Ths is my solution. Few things to learn from this. 1)It does not matter
	 * if the length of the string is odd number of even number. 2)Both length
	 * strings should not have any character that more than 1 character that
	 * repeats odd number of times.
	 * 
	 * For example in the even string code the string contains unique characters
	 * the odd count is greater than 1. so we should return false.
	 * 
	 * 
	 * Learning 2:
	 * 
	 * I am iterating the map through Foreach. There is a possibility of
	 * concurrent modification exception. Depending on the requirement we have
	 * to use either iterating using for each or
	 * 
	 * class map iteration as follows
	 * 
	 * public static void printMap(Map mp) { Iterator it =
	 * mp.entrySet().iterator(); while (it.hasNext()) { Map.Entry pair =
	 * (Map.Entry)it.next(); System.out.println(pair.getKey() + " = " +
	 * pair.getValue()); it.remove(); // avoids a
	 * ConcurrentModificationException } }
	 * 
	 */
	public boolean canPermutePalindrome(String s) {
		int length = s.length();
		if (length <= 1) {
			return true;
		}

		HashMap<Character, Integer> map = new HashMap<Character, Integer>();

		for (Character c : s.toCharArray()) {
			if (map.containsKey(c)) {
				map.put(c, map.get(c) + 1);

			} else {
				map.put(c, 1);
			}

		}
		int oddcount = 0;
		for (Character c : map.keySet()) {

			int value = map.get(c);
			if (value % 2 != 0) {
				oddcount++;
			}

		}
		if (oddcount > 1) {
			return false;
		}

		return true;
	}

	/*
	 * 
	 * A better looking solution with same time complexity
	 * 
	 * 
	 */

	public boolean canPermutePalindromeBetter(String s) {
		HashSet<Character> set = new HashSet<Character>();
		for (Character c : s.toCharArray()) {
			if (!set.add(c)) {
				set.remove(c);
			}

		}
		if (set.size() > 1)
			return false;
		return true;
	}
}
