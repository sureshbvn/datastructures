import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
    	int[] output=new int[2];
    	HashMap<Integer,Integer> map=new HashMap<Integer,Integer>();
    	for(int i=0;i<nums.length;i++){
    		if(map.containsKey(nums[i])){
    			int start=map.get(nums[i]);
    			int end=i+1;
    			output[0]=start+1;
    			output[1]=end;
    			
    		}
    		else{
    			map.put(target-nums[i], i);
    		}
    	}
    	return output;
    	}
    public static void main(String args[]){
    	int[] numbers={3,2,4};
    	int target=6;
    	TwoSum t=new TwoSum();
    	int[] values=t.twoSum(numbers, target);
    	System.out.println("The value 0 is"+values[0]+"and value1 is"+values[1]);
    }
}