package Arrays;


/***
 * 
 * http://www.careercup.com/question?id=5758677862580224
 * 
 * Given an unsorted array of integers find a minimum number which is not present in array. 
     e.g -1 ,4, 5, -23,24 is array then answer should be -22.
 */
import java.util.HashSet;

public class SmallestMissing {

	public int minNonArrayNum(int[] num){
		int size = num.length;
		boolean bool[] = new boolean[size];
		bool[0] = true;
		int minimum = num[0];
		for (int i = 0 ; i < size ; i++) {
		if (num[i] < minimum) {
		minimum = num[i];
		}
		}
		for (int i = 0 ; i < size ; i++) {
		if (num[i]-minimum < size) {
		bool[num[i]-minimum] = true;
		}
		}

		for (int i = 1 ; i < size ; i++) {
		if (!bool[i]) {
		return i+minimum;
		}
		
		}
		return 0;
	}
		
	public static void main(String args[]){
	    int[] a={0,3,2,1,-2};
	    int max=Integer.MIN_VALUE;
	    int min=Integer.MAX_VALUE;
	    HashSet<Integer> set=new HashSet<Integer>();
	    for(int i=0;i<a.length;i++){
	        if(a[i]<min)
	            min=a[i];
	        if(a[i]>max)
	            max=a[i];
	        set.add(a[i]);
	        
	    }
	   /* for(int i=min;i<max;i++){
	        if(!set.contains(i)){
	           System.out.println(i);
	           break;
	        }
	            
	    }*/
	    System.out.println(new SmallestMissing().minNonArrayNum(a));
	}
	
	
}
